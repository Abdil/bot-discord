package com.discord.bots;

import com.discord.bots.commands.Ping;
import com.discord.bots.secret.Token;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

public class Main {
    public static String Prefix = ".";
    public static void main(String[] args){
        DiscordApi Bot = new DiscordApiBuilder().setToken(Token.token).login().join();

        Bot.addListener(new Ping());

        System.out.println("Bot is online!" + "Owner of the server" + Bot.getOwner());
    }
}
